// Example use of lambda functions. Also uses uniform initialization
#include <iostream>
#include <vector>
#include <typeinfo>

using intVector = std::vector<int>;
using floatVector = std::vector<float>;
using charVector = std::vector<char>;

template <typename T1, typename T2>
void processVector(T1 v, const std::function<void(T2)>& lambda) {
    std::for_each(v.begin(), v.end(), lambda);
}

int main() {
    intVector v {1,2,3,4,5};
    floatVector f {1.22, 2.5f, 3.3};
    charVector c {'a', 'q', 'u', 'z', 'r'};

    double sum = 0.0;

    auto lambdaF = [](auto&& element) {
        std::cout << element << std::endl;
    };

    auto counter = [&sum](auto&& element) {
        sum +=  element;
    };

    std::cout << "Processing int vector" << std::endl;
    processVector<intVector, int>(v, lambdaF);
    processVector<intVector, int>(v, counter);
    std::cout << "Sum : " << sum << std::endl;

    sum = 0.0;

    std::cout << "Processing float vector" << std::endl;
    processVector<floatVector, float>(f, lambdaF);
    processVector<floatVector, float>(f, counter);
    std::cout << "Sum : " << sum << std::endl;

    sum = 0.0;

    std::cout << "Processing char vector" << std::endl;
    processVector<charVector, char>(c, lambdaF);
    processVector<charVector, char>(c, counter);
    std::cout << "Sum : " << sum << std::endl;

    return 0;
}
