// Trying to make sure that the C++ 20 compiler is installed properly
#include <iostream>
#include <string>

int main(void) {
    using namespace std::string_literals;

    auto output = "Hello World!"s;
    std::cout << output << std::endl;

    return 0;
}
