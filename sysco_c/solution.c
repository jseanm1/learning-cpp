#include <stdio.h>

struct book{
    char name;
    int pages;
};

int main(int argc, char** argv) {
    struct book bookArray[9];

    // Let's declare the data
    bookArray[0].name = 'A';
    bookArray[1].name = 'B';
    bookArray[2].name = 'C';
    bookArray[3].name = 'D';
    bookArray[4].name = 'E';
    bookArray[5].name = 'F';
    bookArray[6].name = 'G';
    bookArray[7].name = 'H';
    bookArray[8].name = 'I';

    bookArray[0].pages = 108;
    bookArray[1].pages = 78;
    bookArray[2].pages = 89;
    bookArray[3].pages = 10;
    bookArray[4].pages = 25;
    bookArray[5].pages = 43;
    bookArray[6].pages = 12;
    bookArray[7].pages = 11;
    bookArray[8].pages = 18;

    // Let's sort the array the simplest way

    struct book temp;

    for (int i=0; i<9; i++) {
        for (int j=1; j<9; j++) {
            if (bookArray[j-1].pages < bookArray[j].pages) {
                temp = bookArray[j-1];
                bookArray[j-1] = bookArray[j];
                bookArray[j] = temp;
            }
        }
    }

    printf("Books sorted by page size : \n");
    for (int i=0; i<9; i++) {
        printf("%c : %d \n", bookArray[i].name, bookArray[i].pages);
    }

    printf("--------\n");

    int diff[5];
    int min = 108;
    int startIndex = 0;

    printf("Difference of page numbers between book sets of 5\n");
    for (int i=0; i<5; i++) {
        diff[i] = bookArray[i].pages - bookArray[i+4].pages;
        printf("%d \n", diff[i]);
        if (min > diff[i]) {
            min = diff[i];
            startIndex = i;
        }
    }

    printf("Minimum difference : %d\n", min);
    printf("Books to be distributed : \n");

    for (int i=startIndex; i<startIndex+5; i++) {
        printf("Book : %c, Pages : %d \n", bookArray[i].name, bookArray[i].pages);
    }

    return 0;
}